from ncclient import manager
from settings import credentials
import xml.dom.minidom
import xml.etree.ElementTree as ET

netconf_filter = open("../filters/route-filter.xml").read()

with manager.connect(
    host=credentials['host'],
    port="830",
    timeout=30,
    username=credentials['username'],
    password=credentials['password'],
    hostkey_verify=False,
) as m:


    netconf_reply = (m.get(netconf_filter))
    raw = netconf_reply.data_xml
    
    #raw_config = m.get_config(source='running').data_xml
    #tree = ET.ElementTree(raw_config)
    xml = xml.dom.minidom.parseString(raw).toprettyxml(indent="  ")
    #xmlstr = xml.dom.minidom.parseString(ET.tostring(raw_config)).toprettyxml(indent="  ")
    
    file = open("routing_table.xml", "w")
    file.write(xml)
    file.close()