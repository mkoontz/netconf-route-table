from operator import contains
from ncclient import manager
import xmltodict
from settings import credentials
from tabulate import tabulate
from devices import devices


#host = credentials['host']
netconf_filter = open("../filters/route-filter.xml").read()
route_list = []

                        
#def update_route_table(routes):
#    sql = "INSERT INTO rt(vrf,af,out,protocol,prefix,nexthop) VALUES (%s)"
#    conn = None
#
#    try:
#
#        #params = config()
#        #conn = psycopg2.connect(**params)
#        conn = psycopg2.connect(database="routetable", user='cisco', password='cisco', host='192.168.1.234', port= '5432')
#        cur = conn.cursor()
#        query = ''' INSERT INTO rout VALUES (1,%(rtable)s, %(af)s,%(out)s,%(protocol)s,%(pfx)s,%(nhop)s) '''
#        execute_batch(cur, query, routes)
#        #cur.executemany(sql,route_list)
#        conn.commit()
#        cur.close()
#    
#    except (Exception, psycopg2.DatabaseError) as error:
#        print(error)
#    finally:
#        if conn is not None:
#            conn.close()


for device in devices:

    host = device

    with manager.connect(
        host=device,
        port="830",
        timeout=30,
        username=credentials['username'],
        password=credentials['password'],
        hostkey_verify=False,
    ) as m:

        netconf_reply = (m.get(netconf_filter))

        tables = xmltodict.parse(netconf_reply.xml)["rpc-reply"]["data"]['routing-state']['routing-instance']
        
        for table in tables:
            vrf = table["name"]

            if "Platform_iVRF" not in vrf:
                ribs = table["ribs"]["rib"]
                for rib in ribs:
                    af = rib["address-family"]
                    try:                    
                        rt = rib["routes"]["route"]
                    except: 
                        continue
                    
                    if isinstance(rt, dict):
                        dic = {}
                        prefix = rt["destination-prefix"]
                        ogi = rt["next-hop"]["outgoing-interface"]
                        nh = rt["next-hop"]["next-hop-address"]
                        proto = rt["source-protocol"]

                        if type(proto) != str:
                            protocol = proto["#text"]
                            dic["rtable"] = vrf
                            dic["af"] = af
                            dic["pfx"] = prefix
                            dic["out"] = ogi
                            dic["nhop"] = nh
                            dic["host"] = host    
                            dic["protocol"] = protocol
                            route_list.append(dic)          
                        else:
                            protocol = proto
                            dic["rtable"] = vrf
                            dic["af"] = af
                            dic["pfx"] = prefix
                            dic["out"] = ogi
                            dic["nhop"] = nh
                            dic["host"] = host    
                            dic["protocol"] = protocol   
                            route_list.append(dic)    
                    else:
                        for r in rt:
                            dic = {}
                            if type(r) != str:
                                prefix = r["destination-prefix"]
                                ogi = r["next-hop"]["outgoing-interface"]
                                nh = r["next-hop"]["next-hop-address"]
                                proto = r["source-protocol"]

                                if type(proto) != str:
                                    protocol = proto["#text"]
                                    dic["rtable"] = vrf
                                    dic["af"] = af
                                    dic["pfx"] = prefix
                                    dic["out"] = ogi
                                    dic["nhop"] = nh
                                    dic["host"] = host    
                                    dic["protocol"] = protocol
                                    route_list.append(dic)             
                                else:
                                    protocol = proto
                                    dic["rtable"] = vrf
                                    dic["af"] = af
                                    dic["pfx"] = prefix
                                    dic["out"] = ogi
                                    dic["nhop"] = nh
                                    dic["host"] = host    
                                    dic["protocol"] = protocol   
                                    route_list.append(dic)     
                            else:
                                continue

    #update_route_table(route_list)

    # Need to decide on a DBMS before moving this to production. Future evaluations of MariaDB and MongoDB. 
    #pprint.pprint(route_list)

    table = list()
    headers = ["Source Host", "Address-family", "VRF", "Protocol", "Prefix", "Next Hop", "Outgoing Interface"]            

    for item in route_list:
        src = item["host"]
        add = item["af"]
        vr = item["rtable"]
        if "bgp" in item["protocol"]:
            pro = "BGP"
        elif "ospf" in item["protocol"]:
            pro = "OSPF"
        elif "eigrp" in item["protocol"]:
            pro = "EIGRP"
        else:
            pro = item["protocol"]
        #pro = item["protocol"]
        p = item["pfx"]
        b = item["nhop"]
        c = item["out"]
    
        table.append((src, add, vr, pro, p, b, c))

print(tabulate(table, headers, tablefmt="fancy_grid"))





