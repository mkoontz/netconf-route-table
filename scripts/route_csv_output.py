from operator import contains
from ncclient import manager
import xmltodict
from settings import credentials
import csv
import datetime
from devices import devices

today = datetime.date.today()
now = datetime.datetime.now()


netconf_filter = open("../filters/route-filter.xml").read()
route_list = []

for device in devices:                        
    host = device
    with manager.connect(
        host=device,
        port="830",
        timeout=30,
        username=credentials['username'],
        password=credentials['password'],
        hostkey_verify=False,
    ) as m:

        netconf_reply = (m.get(netconf_filter))

        tables = xmltodict.parse(netconf_reply.xml)["rpc-reply"]["data"]['routing-state']['routing-instance']
        
        for table in tables:
            vrf = table["name"]

            if "Platform_iVRF" not in vrf:
                ribs = table["ribs"]["rib"]
                for rib in ribs:
                    af = rib["address-family"]
                    try:                    
                        rt = rib["routes"]["route"]
                    except: 
                        continue
                    
                    if isinstance(rt, dict):
                        dic = {}
                        prefix = rt["destination-prefix"]
                        ogi = rt["next-hop"]["outgoing-interface"]
                        nh = rt["next-hop"]["next-hop-address"]
                        proto = rt["source-protocol"]

                        if type(proto) != str:
                            protocol = proto["#text"]
                            dic["rtable"] = vrf
                            dic["af"] = af
                            dic["pfx"] = prefix
                            dic["out"] = ogi
                            dic["nhop"] = nh
                            dic["host"] = host    
                            dic["protocol"] = protocol
                            route_list.append(dic)          
                        else:
                            protocol = proto
                            dic["rtable"] = vrf
                            dic["af"] = af
                            dic["pfx"] = prefix
                            dic["out"] = ogi
                            dic["nhop"] = nh
                            dic["host"] = host    
                            dic["protocol"] = protocol   
                            route_list.append(dic)    
                    else:
                        for r in rt:
                            dic = {}
                            if type(r) != str:
                                prefix = r["destination-prefix"]
                                ogi = r["next-hop"]["outgoing-interface"]
                                nh = r["next-hop"]["next-hop-address"]
                                proto = r["source-protocol"]

                                if type(proto) != str:
                                    protocol = proto["#text"]
                                    dic["rtable"] = vrf
                                    dic["af"] = af
                                    dic["pfx"] = prefix
                                    dic["out"] = ogi
                                    dic["nhop"] = nh
                                    dic["host"] = host    
                                    dic["protocol"] = protocol
                                    route_list.append(dic)             
                                else:
                                    protocol = proto
                                    dic["rtable"] = vrf
                                    dic["af"] = af
                                    dic["pfx"] = prefix
                                    dic["out"] = ogi
                                    dic["nhop"] = nh
                                    dic["host"] = host    
                                    dic["protocol"] = protocol   
                                    route_list.append(dic)     
                            else:
                                continue


header = ['source', 'address-family', 'VRF', 'Protocol', 'Prefix', 'next-hop', 'outgoing-interface']

with open("../output/route-table" + "-" + today.strftime("%m%d%y") + "-" + now.strftime("%H%M%S") + ".csv", 'w', encoding='UTF8') as csv_file:
    writer = csv.writer(csv_file)
    writer.writerow(header)
    for item in route_list:
        row_data =[]
        src = item["host"]
        add = item["af"]
        vr = item["rtable"]
        if "bgp" in item["protocol"]:
            pro = "BGP"
        elif "ospf" in item["protocol"]:
            pro = "OSPF"
        elif "eigrp" in item["protocol"]:
            pro = "EIGRP"
        else:
            pro = item["protocol"]
        #pro = item["protocol"]
        p = item["pfx"]
        b = item["nhop"]
        c = item["out"]
        row_data = [src, add, vr, pro, p, b, c]
        writer.writerow(row_data)





